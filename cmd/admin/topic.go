package admin

import (
	"log"

	"github.com/spf13/cobra"
	. "gitlab.com/lootved/cli-template/cfg"
)

func topicCmd() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "topic",
		Short: "List, create or delete topics",
		Long:  "Manage the topics and cluster config",
	}
	cmd.AddCommand(createTopicCmd())
	cmd.AddCommand(listTopicCmd())
	return cmd
}

func listTopicCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "list",
		Short: "List all the topic",
		Long:  "List the topics of cluster defined by the positional flags",
		Args:  cobra.MaximumNArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			server := Cfg.Host + ":" + Cfg.Port
			log.Println("Listing topics of", server)
		},
	}
}

func createTopicCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "create topicName...",
		Short: "Create topics",
		Long:  "Create any topic passed as argument",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			server := Cfg.Host + ":" + Cfg.Port
			for _, topic := range args {
				log.Println("Creating topic", "'"+topic+"'", "on boostrapserver", server)
			}
		},
	}
}
