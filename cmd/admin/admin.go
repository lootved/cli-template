package admin

import "github.com/spf13/cobra"

func Init() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "admin",
		Short: "Administrate the cluster",
		Long:  "Manage the topics and cluster config",
	}
	cmd.AddCommand(topicCmd())
	return cmd
}
