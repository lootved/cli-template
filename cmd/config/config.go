package config

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/lootved/cli-template/cfg"
)

func Init() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "config [outputPath]",
		Short: "Save the current/default configuration to a file or stdout using yaml format",
		Long:  "Export current configuration to a file or stdout if no argument is provided",
		Args:  cobra.MaximumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			data := cfg.Export()
			outputFile := "/dev/stdout"
			if len(args) != 0 {
				outputFile = args[0]
			}
			f, err := os.Create(outputFile)
			if err != nil {
				log.Fatalln(err)
			}
			defer f.Close()
			f.Write(data)
		},
	}

	return cmd
}
