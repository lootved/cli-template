package main

import (
	"log"
	"os"

	"gitlab.com/lootved/cli-template/cfg"
	. "gitlab.com/lootved/cli-template/cfg"
	"gitlab.com/lootved/cli-template/cmd/admin"
	"gitlab.com/lootved/cli-template/cmd/config"
	"gitlab.com/lootved/cli-template/cmd/push"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cmd = cobra.Command{
		Use:   "cli",
		Short: "Short decription of the cli",
		Long:  "Long description of the cli",
		//	Run: func(cmd *cobra.Command, args []string) { },
	}
)

func init() {

	cmd.AddCommand(config.Init())
	cmd.AddCommand(admin.Init())
	cmd.AddCommand(push.Init())

	cmd.PersistentFlags().StringVarP(&Cfg.Host, "server", "s", "kafka", "bootstrap server FQDN")
	cmd.PersistentFlags().StringVarP(&Cfg.Port, "port", "p", "9092", "port of the bootstrap server")
	cmd.PersistentFlags().StringVarP(&Cfg.Topic, "topic", "t", "messages", "topic to target")
	viper.BindPFlags(cmd.Flags())
	cfg.InitViper()

}

func main() {

	if err := cmd.Execute(); err != nil {
		log.Println(err)
		os.Exit(1)
	}
}
